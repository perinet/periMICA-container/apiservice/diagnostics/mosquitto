# apiservice diagnostics/mosquitto

The diagnostics/mosquitto apiservice parses the log file of a mosquitto
mqtt broker for connected clients. A list of those connected clients
can be fetched from resource `/diagnostics/mosquitto/clients`.

## Example

```json
[
    {
        "hostname":"periCORE-hs79u.local",
        "id":"auto-E466A205-52D3-7FB8-7143-7629ADC6E5D0",
        "ip":"fe80::742e:db78:77f2:0",
        "timestamp":"2024-04-22T07:11:36"
    },
    {
        "hostname":"periCORE-krw4h.local",
        "id":"auto-2F2C3855-CECE-9DC3-B588-DCF1753E02E4",
        "ip":"fe80::742e:dba7:d347:0",
        "timestamp":"2024-04-22T07:11:36"
    },
    {
        "hostname":"dashboard-perimica-bjxt4.local",
        "id":"dashboard-perimica-bjxt4",
        "ip":"fe80::a:ed17:67ed:8026",
        "timestamp":"2024-04-23T07:09:24"
    }
]
```

## Dual License

This software is by default licensed via the GNU Affero General Public License
version 3. However it is also available with a commercial license on request
(https://perinet.io/contact).
