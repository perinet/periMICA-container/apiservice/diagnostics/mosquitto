module gitlab.com/perinet/periMICA-container/apiservice/diagnostics/mosquitto

go 1.18

require (
	github.com/godbus/dbus/v5 v5.1.0
	github.com/holoplot/go-avahi v1.0.1
	gitlab.com/perinet/generic/lib/httpserver v0.0.0-20240304113050-0340948978be
)

require (
	github.com/alexandrevicenzi/go-sse v1.6.0 // indirect
	github.com/felixge/httpsnoop v1.0.1 // indirect
	github.com/google/uuid v1.3.0 // indirect
	github.com/gorilla/handlers v1.5.1 // indirect
	github.com/gorilla/mux v1.8.0 // indirect
	golang.org/x/exp v0.0.0-20230510235704-dd950f8aeaea // indirect
)
