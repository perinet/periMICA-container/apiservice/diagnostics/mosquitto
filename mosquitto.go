/*
 * Copyright (c) Perinet GmbH
 * All rights reserved
 *
 * This software is dual-licensed: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 as
 * published by the Free Software Foundation. For the terms of this
 * license, see http://www.fsf.org/licensing/licenses/agpl-3.0.html,
 * or contact us at https://server.io/contact/ when you want license
 * this software under a commercial license.
 */

// Package apiservice_diagnostics_mosquitto implements the RESTful API to the
// mosquitto diagnostics service
package apiservice_diagnostics_mosquitto

import (
	"bufio"
	"encoding/json"
	"io"
	"log"
	"net/http"
	"os"
	"strings"
	"sync"
	"time"

	"github.com/godbus/dbus/v5"
	"github.com/holoplot/go-avahi"

	server "gitlab.com/perinet/generic/lib/httpserver"
	rbac "gitlab.com/perinet/generic/lib/httpserver/rbac"
)

type config struct {
	LogFilePath string `json:"mosquitto_log"`
}

type client struct {
	Hostname  string `json:"hostname"`
	Id        string `json:"id"`
	Ip        string `json:"ip"`
	Timestamp string `json:"timestamp"`
}

var (
	logger       log.Logger = *log.Default()
	config_cache            = config{
		LogFilePath: "/var/log/mosquitto.log",
	}
	clients      map[string]client
	clients_lock sync.Mutex

	dbus_connection *dbus.Conn
	avahi_server    *avahi.Server
)

func tail(filename string, ch chan string) {
	f, err := os.Open(filename)
	if err != nil {
		panic(err)
	}
	defer f.Close()
	r := bufio.NewReader(f)
	info, err := f.Stat()
	if err != nil {
		panic(err)
	}
	oldSize := info.Size()
	var transmission string
	for {
		for {
			res, err := r.ReadString('\n')
			transmission += res
			if err == io.EOF {
				break
			}
			last_char := transmission[len(transmission)-1:]
			if last_char == "\n" {
				ch <- transmission
				transmission = ""
			}
		}
		pos, err := f.Seek(0, io.SeekCurrent)
		if err != nil {
			panic(err)
		}
		for {
			time.Sleep(time.Millisecond * 10)
			newinfo, err := f.Stat()
			if err != nil {
				panic(err)
			}
			newSize := newinfo.Size()
			if newSize != oldSize {
				if newSize < oldSize {
					f.Seek(0, 0)
				} else {
					f.Seek(pos, io.SeekStart)
				}
				r = bufio.NewReader(f)
				oldSize = newSize
				break
			}
		}
	}
}

func broker_start_detect(log string) bool {
	const prefix = "mosquitto version"
	const suffix = "running\n"
	if strings.HasPrefix(log, prefix) && strings.HasSuffix(log, suffix) {
		return true
	}
	return false
}

func add_client(log string, timestamp string) bool {
	const prefix = "New client connected from "
	if strings.HasPrefix(log, prefix) {
		// determine client IP
		tmp := strings.Split(strings.TrimPrefix(log, prefix), " ")

		// remove port
		idx := strings.LastIndex(tmp[0], ":")
		logger.Printf("index of last ':': %d\n", idx)

		var cl client
		cl.Ip = tmp[0][:idx]
		logger.Printf("ip: %s\n", cl.Ip)

		// resolve host name by address
		if avahi_server != nil {
			res, err := avahi_server.ResolveAddress(avahi.InterfaceUnspec, avahi.ProtoInet6, cl.Ip, 0)
			if err != nil {
				logger.Printf("Unable to resolve hostname od address %s", cl.Ip)
			} else {
				cl.Hostname = res.Name
			}
		}
		cl.Id = tmp[2]
		cl.Timestamp = timestamp
		clients_lock.Lock()
		clients[cl.Id] = cl
		clients_lock.Unlock()
		return true
	}
	return false
}

func remove_client(log string) bool {
	const prefix = "Client "
	var suffix = [...]string{
		" closed its connection.\n",
		" disconnected.\n",
		" disconnected, not authorised.\n",
		" disconnected: Protocol error.\n",
		" has exceeded timeout, disconnecting.\n",
	}

	if strings.HasPrefix(log, prefix) {
		for _, entry := range suffix {
			if strings.HasSuffix(log, entry) {
				// determine id
				id := strings.TrimPrefix(strings.TrimSuffix(log, entry), prefix)
				clients_lock.Lock()
				delete(clients, id)
				clients_lock.Unlock()
			}
		}
	}
	return false
}

func parse_mosquitto_log(ch chan string) {
	for {
		var rec = <-ch
		// split timestamp/log
		i := strings.Index(rec, " ")
		timestamp := strings.TrimSuffix(rec[:i], ":")
		log := rec[i+1:]

		res := broker_start_detect(log)
		if res {
			logger.Printf("Broker restarted (%s).", timestamp)

			// clear clients map
			clients_lock.Lock()
			clients = make(map[string]client)
			clients_lock.Unlock()

			continue
		}
		res = add_client(log, timestamp)
		if res {
			continue
		}
		res = remove_client(log)
		if res {
			continue
		}
		logger.Print(log)
	}
}

func init() {
	logger.SetPrefix("mosquitto-log: ")
	logger.Print("starting")

	var err error
	dbus_connection, err = dbus.SystemBus()
	if err != nil {
		logger.Printf("Cannot get system bus: %v", err)
	} else {
		avahi_server, err = avahi.ServerNew(dbus_connection)
		if err != nil {
			logger.Printf("Avahi new failed: %v", err)
		}
	}

	ch := make(chan string)
	go tail(config_cache.LogFilePath, ch)
	go parse_mosquitto_log(ch)
}

func PathsGet() []server.PathInfo {
	return []server.PathInfo{
		{Url: "/diagnostics/mosquitto/clients", Method: server.GET, Role: rbac.NONE, Call: clients_get},
	}
}

func clients_get(w http.ResponseWriter, r *http.Request) {
	clients_lock.Lock()
	// convert clients map into slice of clients
	var tmp []client
	for _, entry := range clients {
		tmp = append(tmp, entry)
	}
	clients_lock.Unlock()

	res, err := json.Marshal(tmp)
	if err != nil {
		logger.Print(err)
		w.WriteHeader(http.StatusInternalServerError)
		w.Write(json.RawMessage(`{"error": "cannot marshal object to json"}`))
	} else {
		w.WriteHeader(http.StatusOK)
		w.Write(res)
	}
}
